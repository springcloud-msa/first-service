package com.example.firstservice.controller;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/first-service")
@Slf4j
public class FirstController {
	Environment env;
    
    //DI(Dependency Injection) - Constructor args
    public FirstController(Environment env) {
		this.env = env;
	}

	@GetMapping("/health-check")
	public String status(HttpServletRequest request) {
		return String.format("%s Connected!! by port %s "
								, env.getProperty("spring.application.name") 
								, request.getServerPort() );
	}
	
	@GetMapping("/message")
	public String message(@RequestHeader("first-request") String header) {
		log.info("=====first-reuqest : "+header);
		return "Welcome First Service!";
	}
}







